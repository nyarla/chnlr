package Chnlr::Template;

use strict;
use Any::Moose '::Role';
use Chnlr::Exceptions;
use namespace::clean -except => [qw( meta )];

requires qw( render exists stat );

1;

=head1 NAME

Chnlr::Template - Interface definition for Template renderer

=head1 SYNPOSIS

    package MyTemplate;
    
    use strict;
    use Any::Moose;
    
    with 'Chnlr::Template';
    
    sub render {
        my ( $self, $path, @vars ) = @_;
        # your code here
    }
    
    sub exists {
        my ( $self, $path ) = @_;
        # your code here
    }
    
    sub stat {
        my ( $self, $path ) = @_;
        # your code here
    }
    
    __PACKAGE__->meta->make_immutable;

=head1 DESCRIPTION

This role is interface definition for template renderer.

=head1 INTREFACE METHODS

=head2 new

    my $renderer = MyTemplate->new

This method is constructor of template renderer.

=head2 render

    my $result = $renderer->render( $path, @vars )

This method renders template file designated by argument.

Arguments:

=over

=item C<$path>

A template path is designated.

=item C<@vars>

Template variables are designated.

This arguments is optional.

=back

=head2 exists

    my $bool = $renderer->exists( $path )

This method returns whether a template exists.

=head2 stat

    my $stat = $renderer->stat($path);
    
    $stat->{'exists'}       # 1 or 0
    $stat->{'created'}      # ctime
    $stat->{'lastmodified'} # mtime

This method returns a template status.

=head1 REQUIREMENT METHODS

=head2 render

    sub render {
        my ( $self, $path, @vars ) = @_;
        # your code here
        return $result;
    }

This method receives a template path and template variables,
and returns rendering result.

=head2 exists

    sub exists {
        my ( $self, $path ) = @_;
        # your code here
        return 1 or 0;
    }

This method returns whether a template path exists.

=head2 stat

    use File::stat;
    
    sub stat {
        my ( $self, $path ) = @_;
    
        if ( $self->exists($path) ) {
            return {
                exists          => 1,
                created         => stat($path)->ctime,
                lastmodified    => stat($path)->mtime,
            }
        }
        else {
            return {
                exists => 0,
            }
        }
    }

This method returns a template status.

Result parameters:

=over

=item C<exists>

This value folds whether a template exists.

This value is 1 or 0.

This value is required.

=item C<created>

This value folds created time of a template.

This value is unixtime.

This value is optional.

=item C<lastmodified>

This value folds last modified time of a template.

This value is unixtime.

This value is optional.

=back

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
