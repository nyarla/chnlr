package Chnlr::Types;

use strict;
use Any::Moose;
use Any::Moose (
    'X::Types'                  => [ -declare => [qw(
        Config Server
    )] ],
    'X::Types::' . any_moose()  => [qw( Str ArrayRef HashRef CodeRef )],
);

my @classes = qw(
    Chnlr::Config
    Chnlr::Server
);

for my $class ( @classes ) {
    class_type($class) if ( ! find_type_constraint($class) );
}

# Config
subtype Config,
    as 'Chnlr::Config',
;

coerce Config,
    from Str,
        via { Chnlr::Config->new($_) },
    from ArrayRef[Str],
        via { Chnlr::Config->new($_) },
    from ArrayRef[HashRef],
        via {
            my $config = Chnlr::Config->new;
               $config->merge( $_ ) for @{ $_ };
            return $config;
        },
    from ArrayRef[Str|HashRef],
        via {
            my $config = Chnlr::Config->new;
               $config->merge( $_ ) for map { Chnlr::Config->new($_) } @{ $_ };
            return $config
        },
    from HashRef,
        via { Chnlr::Config->new( $_ ) },
    from CodeRef,
        via { Chnlr::Config->new( $_ ) },
;

# Server
subtype Server,
    as 'Chnlr::Server',
;

coerce Server,
    from HashRef,
        via {
            Chnlr::Server->new(%{ $_ }),
        },
;

for my $class ( @classes ) {
    Any::Moose::load_class($class)
        if ( ! Any::Moose::is_class_loaded($class) );
}

__PACKAGE__->meta->make_immutable;

=head1 NAME

Chnlr::Types - (Moose|Mouse) types for Chnlr.

=head1 SYNPOSIS

    package MyClass;
    
    use strict;
    use Any::Moose;
    use Chnlr::Types qw( Config );
    
    has config => (
        is      => 'rw',
        isa     => Config,
        coerce  => 1,
    );
    
    no Any::Moose;
    __PACKAGE__->meta->make_immutable;

=head1 DESCRIPTION

This class is (Moose|Mouse) type folder.

=head1 TYPES

=head2 Config - type of L<Chnlr::Config>

This type is type of L<Chnlr::Config>.

coerce:

=over

=item Str, ArrayRef[Str], HashRef, CodeRef,

When these types was passed to this rule,
this rule passes these arguments to L<Chnlr::Config> constructor.

=item ArrayRef[HashRef]

When ArrayRef[HashRef] was passed to this rule,
this rule makes L<Chnlr::Config> instance,]
and merges hash references.

=item ArrayRef[Str|HashRef]

When ArrayRef[Str|HashRef] wass passed to this rule,
this rule makes L<Chnlr::Config> instance from Str|HashRef,
and merges.

=back

=head2 Server - type of L<Chnlr::Server>

This type is type of L<Chnlr::Server>.

coerce:

=over 

=item HashRef

When HashREf was passed to this rule,
this rule makes L<Chnlr::Server> instance from HashRef.

=back

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<>gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
