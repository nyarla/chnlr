package Chnlr::Application;

use strict;
use Any::Moose '::Role';
use namespace::clean -excpet => [qw( meta )];

requires qw( run );

sub setup {}

sub to_app {
    my ( $app, @args ) = @_;
    $app = $app->new( @args ) if ( ! ref $app );
    $app->setup;

    return sub {
        my ( $env ) = @_;
        return $app->run( $env );
    }
}


1;

=head1 NAME

Chnlr::Application - Application interface definition for Chnlr.

=head1 SYNPOSIS

    package MyApp;
    
    use strict;
    use Any::Moose;
    
    with 'Chnlr::Application';
    
    sub run {
        my ( $app ) = @_;
        # your application code here
    }
    
    no Any::Moose;
    __PACKAGE__->meta->make_immutable;

=head1 DESCRIPTION

This role is application interface definition for Chnlr.

=head1 INTERFACE METHODS

=head2 new

    my $app = MyApp->new;

This method is constructor.

=head2 setup

    $app->setup;

This method setups application.

=head2 run

    $app->run( $env );

This method runs application

=head2 to_app

    MyApp->to_app
    
    # or
    
    MyApp->new->to_app

=head2 L<Chnlr::Context> properties

This role inherits L<Chnlr::Context>, so you can call L<Chnlr::Context> properties.

=head1 REQUIREMENT METHODS

=head2 run

This role requires C<run> method.

This method receives the PSGI environment hash reference,
and needs to return PSGI response.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
