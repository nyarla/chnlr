package Chnlr::Config;

use strict;
use warnings;

use Chnlr::Exceptions;

use parent qw( Chnlr::Variables );

sub new {
    my $class   = shift;
    my $data    = shift;

    my $files = [];
    my $hash  = $class->SUPER::new({});

    if ( ref $data eq 'CODE' ) {
        $data = $data->();
    }

    if ( ! defined $data ) {
        # pass
    }
    elsif ( ! ref $data ) {
        $files = [ $data ];
    }
    elsif ( ref $data eq 'ARRAY' ) {
        $files = $data;
    }
    elsif ( ref $data eq 'HASH' ) {
        $hash->merge( $data );
    }
    else {
        Chnlr::Exception::NotSupported->throw( error => "Unsupport type: ${data}" );
    }

    if ( @{ $files } > 0 ) {
        for my $file ( @{ $files } ) {
            Chnlr::Exceoptin::NotSupported->throw( error => "Unsupport file name: ${file}" )
                if ( ref $file );
            Chnlr::Exception::FileIOError->throw( error => "File does not exists: ${file}" )
                if ( ! -e $file );
            Chnlr::Exception::FileIOError->throw( error => "Cannot read file: ${file}" )
                if ( ! -r $file );

            local $@;
            my $config = do $file;

            Chnlr::Exception::EvaluateFailed->throw( error => "Failed to load configuration file: ${file}: ${@}" )
                if ( $@ );
            Chnlr::Exception::InvalidParameter->throw( error => "Configuration data is not HASH reference: ${file}: ${config}" )
                if ( ref $config ne 'HASH' );

            $hash->merge( $config );
        }
    }

    return bless $hash, $class;
}

1;

=head1 NAME

Chnlr::Config - Config object for Chnlr

=head1 SYNPOSIS

    use Chnlr::Config;
    
    my $conf = Chnlr::Config->new;
    my $conf = Chnlr::Config->new($filename);
    my $conf = Chnlr::Config->new([ $filenameA, $filenameB ]);
    my $conf = Chnlr::Config->new(\%config);
    my $conf = Chnlr::Config->new(\&config_generator);

=head1 DESCRIPTION

This class is configuration hash object class for Chnlr.

This class inherits L<Chnlr::Variables>.

=head1 METHODS

=head2 new

    my $conf = Chnlr::Config->new;
    my $conf = Chnlr::Config->new($filename);
    my $conf = Chnlr::Config->new([ $filenameA, $filenameB ]);
    my $conf = Chnlr::Config->new(\%config);
    my $conf = Chnlr::Config->new(\&config_generator);

This method is constructor of Chnlr::Config.

The following can be passed to an argument:

=over

=item undef

When undef was passed to an argument, the empty HASH reference is blessed.

=item string

When string was passed to an argument, this method tries reading designated file.

Argument has to be the filename of the Perl script to which HASH reference is returned.

=item ARRAY reference

When ARRAY reference passed to an argment, this method tries reading files designated by ARRAY reference.

Argument has to be the filename of the Perl script to which HASH reference is returned.

=item HASH reference

When HASH reference passed to an argumet, this method blesses designated HASH reference.

=item CODE reference

When CODE reference passed ad argument, this method executes CODE reference,
And this method processes CODE reference result.

A result of CODE reference is processed with a way above-mentioned.

=back

=head2 Another methods.

This class is inherits L<Chnlr::Variables>, so you can call L<Chnlr::Variables> methods.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
