package Chnlr::Context;

use strict;
use Any::Moose;
use Chnlr::Types qw( Config Server );
use Chnlr::Exceptions;
use namespace::clean -except => [qw( meta )];

has config => (
    is          => 'rw',
    isa         => Config,
    coerce      => 1,
    lazy_build  => 1,
);

sub _build_config {
    my ( $self ) = @_;

    if ( exists $ENV{'CHNLR_CONFIG'} ) {
        return $ENV{'CHNLR_CONFIG'};
    }

    Chnlr::Exception::ParameterMissing->throw(
        error => q[Configuration file is not designated. Please set $ENV{'CHNLR_CONFIG'}]
    );
}

has server => (
    is          => 'ro',
    isa         => Server,
    coerce      => 1,
    lazy_build  => 1,
);

sub _build_server {
    my ( $self ) = @_;

    if ( exists $self->config->{'server'} ) {
        return $self->config->{'server'};
    }

    return {};
}

sub action {
    my ( $self, %param ) = @_;

    my $controller  = delete $param{'controller'}
        or Chnlr::Exception::ArgumentParameterMissing->throw( error => "Argument 'controller' is missing" );
    my $action      = delete $param{'action'}
        or Chnlr::Exception::ArgumentParameterMissing->throw( error => "Argument 'action' is missing" );

    Any::Moose::load_class($controller)
        if ( ! Any::Moose::is_class_loaded($controller) );

    Chnlr::Exception::NotImplemented->throw( error => "${controller}->${action} is not implemented." )
        if ( ! $controller->can($action) );

    return $controller->$action( $self, %param );
}

__PACKAGE__->meta->make_immutable;

=head1 NAME

Chnlr::Context - Context object for Chnlr.

=head1 SYNPOSIS

    use Chnlr::Context;
    
    my $context = Chnlr::Context->new( config => $file );

=head1 DESCRIPTION

This class is a context object for Chnlr.

=head1 PROPERTIES

=head2 config

This property is accessor of configuration hash.

This property folds L<Chnlr::Config> object.

=head2 server

This property is accessor of server environment variables.

This property folds L<Chnlr::Server> object.

=head1 METHODS

=head2 new

    my $context = Chnlr::Context->new( config => $config );

This method is constructor of L<Chnlr::Config>.

=head2 action

    $context->action( controller => $controller, action => $action, %other_params );

This method executes controller action.

B<Arguments>:

=over

=item C<controller>

A controller name is designated.

This argument is required.

=item C<action>

A controller method is designated.

This argument is required.

=item C<%other_params>

Other arguments which would like to pass the controller to the action are passed.

=back

B<Arguments of Controller action>:

    package MyApp::Controller::Foo;
    
    sub action {
        my ( $controller, $application, %other_params ) = @_;
        # your action code here.
    }

=over

=item C<$controller>

Controler class name.

=item C<$application>

Application context object.

=item C<%other_params>

Other arguments by which it was passed to the action method

=back

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
