package Chnlr::Variables;

use strict;
use warnings;

use Chnlr::Exceptions;

use Hash::Merge::Simple ();
use Data::Rmap ();
use Encode ();

sub new {
    my $class   = shift;
    my $hash    = shift || {};

    Chnlr::Exception::InvalidArgumentParameter->throw( erro => "Argument is not HASH reference." )
        if ( ref $hash ne 'HASH' );

    my $self = bless $hash, $class;

    return $self;
}

sub merge {
    my ( $self, @sources ) = @_;

    %{ $self } = %{ Hash::Merge::Simple->merge( $self, @sources ) };

    return $self;
}

sub visit {
    my $self        = shift;
    my $callback    = shift or Chnlr::Exception::ArgumentParameterMissing->throw( error => "Callback is not designeted." );
    Chnlr::Exception::InvalidArgumentParameter->throw( error => "Callback is not CODE reference" )
        if ( ref $callback ne 'CODE' );

    Data::Rmap::rmap { $_ = $callback->( $_ ) } $self;

    return $self;
}

sub decode {
    my $self        = shift;
    my $encoding    = shift or Chnlr::Exception::ArgumentParameterMissing->throw( error => "Encoding is required." );

    my $enc = Encode::find_encoding($encoding);
    $self->visit(sub { return $enc->decode($_[0]) });

    return $self;
}

sub encode {
    my $self        = shift;
    my $encoding    = shift or Chnlr::Exception::ArgumentParameterMissing->throw( error => "Encoding is required." );

    my $enc = Encode::find_encoding($encoding);
    $self->visit(sub { return $enc->encode($_[0]) });

    return $self;
}

1;

=head1 NAME

Chnlr::Variables - Variables object for Chnlr

=head1 SYNPOSIS

    use Chnlr::Variables;
    
    my $vars = Chnlr::Variables->new({ foo => 'bar' });
    
    print $vars->{'foo'};

=head1 DESCRIPTION

This class is variables class for Chnlr

=head1 METHODS

=head2 new

    my $vars = Chnlr::Variables->new( \%hash );
    my $vars = Chnlr::Variables->new;

This method is constructor of Chnlr::Variables.

The HASH reference is designated as an argument.

=head2 merge

    $vars->merge( \%hashA, \%hashB, ... \%hashN );

This method merges other hash reference.

This method implemented by L<Hash::Merge::Simple>.

=head2 visit

    $vars->visit(sub { lc $_[0] });

This method visits hash tree values.

The CODE reference is designated as an argument.

=head2 decode/encode

    $vars->decode('utf8');
    $vars->encode('utf8');

These methods decodes/encodes hash tree values.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

