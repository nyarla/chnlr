package Chnlr::Exceptions;

use strict;
use warnings;

use parent qw( Exporter );

my ( %E, @E );
our ( @EXPORT_OK, %EXPORT_TAGS );

BEGIN {
    %E = (
        'Chnlr::Exception'                              => {
            description => 'Basical exception for Chnlr',
            alias       => 'throw',
        },
        
        'Chnlr::Exception::EvaluateFailed'              => {
            isa         => 'Chnlr::Exception',
            description => 'Evaluate code is failed.',
            alias       => 'evaluate_failed',
        },
        
        'Chnlr::Exception::InvalidParameter'            => {
            isa         => 'Chnlr::Exception',
            description => 'Parameter is not valid',
            alias       => 'invalid_parameter',
        },
        'Chnlr::Exception::InvalidArgumentParameter'    => {
            isa         => 'Chnlr::Exception::InvalidParameter',
            description => 'Argument parameter is not valid',
            alias       => 'invalid_argument_parameter',
        },

        'Chnlr::Exception::ParameterMissing'            => {
            isa         => 'Chnlr::Exception',
            description => 'Parameter is missing',
            alias       => 'parameter_missing',
        },
        'Chnlr::Exception::ArgumentParameterMissing'    => {
            isa         => 'Chnlr::Exception::ParameterMissing',
            description => 'Argument parameter is missing',
            alias       => 'argument_parameter_missing',
        },

        'Chnlr::Exception::NotSupported'                => {
            isa         => 'Chnlr::Exception',
            description => 'Chnlr does not support',
            alias       => 'not_supported',
        },
        'Chnlr::Exception::NotImplemented'              => {
            isa         => 'Chnlr::Exception',
            description => 'Chnlr is not implemented',
            alias       => 'not_implemented',
        },

        'Chnlr::Exception::IOError'                     => {
            isa         => 'Chnlr::Exception',
            description => 'IO error',
            alias       => 'io_error',
        },
        'Chnlr::Exception::FileIOError'                 => {
            isa         => 'Chnlr::Exception::IOError',
            description => 'File IO Error',
            alias       => 'file_io_error',
        },
    );

    @E = keys %E;

    for my $define ( values %E ) {
        if ( exists $define->{'alias'} ) {
            push @EXPORT_OK, $define->{'alias'};
        }
    }

    %EXPORT_TAGS = ( all => [ @EXPORT_OK ] );
}

use Exception::Class ( %E );

$_->Trace(1) for @E;

1;

=head1 NAME

Chnlr::Exceptions - Exception classes for Chnlr.

=head1 SYNPOSIS

    use Chnlr::Exceptions qw(:all);
    
    throw error => "Basical exception";

=head1 DESCRIPTION

This class is exception definitions for Chnlr.

=head1 EXCEPTIONS

=head2 Chnlr::Exception (alias C<throw>)

=head2 Chnlr::Exception::EvaluateError (alias C<evaluate_failed>)

=head2 Chnlr::Exception::InvalidParameter (alias C<invaid_parameter>)

=head2 Chnlr::Exception::InvalidArgumentParameter (alias C<invalid_argument_parameter>)

=head2 Chnlr::Exception::ParameterMissing (alias C<parameter_missing>)

=head2 Chnlr::Exception::ArgumentParameterMissing (alias C<argument_parameter_missing>)

=head2 Chnlr::Exception::NotSupported (alias C<not_supported>)

=head2 Chnlr::Exception::NotInplemented (alias C<not_implemenetd>)

=head2 Chnlr::Exception::IOError (alias C<io_error>)

=head2 Chnlr::Exception::FileIOError (alias C<file_io_error>)

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
