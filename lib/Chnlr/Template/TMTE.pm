package Chnlr::Template::TMTE;

use strict;
use Any::Moose;
use Text::MicroTemplate::Extended;
use File::stat ();
use Chnlr::Exceptions;
use namespace::clean -except => [qw( meta )];

has config => (
    is          => 'rw',
    isa         => 'HashRef',
    required    => 1,
);

has tmte => (
    is          => 'ro',
    isa         => 'Text::MicroTemplate::Extended',
    lazy_build  => 1,
);

sub _build_tmte {
    my ( $self ) = @_;
    return Text::MicroTemplate::Extended->new( %{ $self->config } );
}

around BUILDARGS => sub {
    my $orig    = shift;
    my $class   = shift;
    my @args    = @_;

    return $class->$orig( config => { @args } );
};

sub render {
    my ( $self, $path, @vars ) = @_;
    return $self->tmte->render_file( $path, @vars );
}

sub find {
    my ( $self, $filename ) = @_;
    my $roots   = $self->tmte->{'include_path'};
    my $ext     = $self->tmte->{'extension'};

    for my $root ( @{ $roots } ) {
        my $path = "${root}/${filename}${ext}";
           $path =~ s{/+}{/}g;
        if ( -e $path && -r $path ) {
            return $path;
        }
    }

    return undef;
}

sub exists {
    my ( $self, $filename ) = @_;

    return ( $self->find($filename) ) ? 1 : 0 ;
}

sub stat {
    my ( $self, $filename ) = @_;
    my $path    = $self->find($filename);
    my $ret     = {};

    if ( ! defined $path ) {
        $ret->{'exists'} = 0;
    }
    else {
        my $stat = File::stat::stat($path);
        Chnlr::Exception::InvalidParameter->throw( error => "File stat response is not defined: ${path}" )
            if ( ! $stat );
        @{ $ret }{qw( created lastmodified exists )}
            = ( $stat->ctime, $stat->mtime, 1 );
    }

    return $ret;
}

__PACKAGE__->meta->make_immutable;

=head1 NAME

Chnlr::Template::TMTE - Template engine using L<Text::MicroTemplate::Exetnded>

=head1 SYNPOSIS

    use Chnlr::Template::TMTE;
    
    my $template = Chnlr::Template::TMTE->new( include_path => ['.'] );
    
    $template->render('filename', @vars);

=head1 SYNPISIS

This class is a template engine using L<Text::MicroTemplate::Extended>.

=head1 CONSTRUCTOR ARGUMENTS

All arguments to a constructor in this class are passed to L<Text::MicroTemplate::Extended>'s constructor.

So, please see L<Text::MicroTemplate::Extended> document about constructor arguments.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Chnlr::Template>

L<Text::MicroTemplate::Extended>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
