package Chnlr::Server;

use strict;
use Any::Moose;
use namespace::clean -except => [qw( meta )];

has request_class => (
    is          => 'ro',
    isa         => 'Str',
    default     => 'Plack::Request',
);

has response_class => (
    is          => 'ro',
    isa         => 'Str',
    default     => 'Plack::Response',
);

has env => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { +{} },
);

has request => (
    is          => 'rw',
    isa         => 'Object',
    lazy_build  => 1,
);

has response => (
    is          => 'rw',
    isa         => 'Object',
    lazy_build  => 1,
);

sub _build_request {
    my $class = $_[0]->request_class;
    Any::Moose::load_class($class)
        if ( ! Any::Moose::is_class_loaded($class) );
    return $class->new( $_[0]->env );
}

sub _build_response {
    my $class = $_[0]->response_class;
    Any::Moose::load_class($class)
        if ( ! Any::Moose::is_class_loaded($class) );
    return $class->new(200);
}

sub req { goto $_[0]->can('request')    }
sub res { goto $_[0]->can('response')   }

__PACKAGE__->meta->make_immutable;

=head1 NAME

Chnlr::Server - Server environment container for Chnlr.

=head1 SYNPOSIS

    use Chnlr::Server;
    
    my $app = sub {
        my $env = shift;
        my $server = Chnlr::Server->new( env => $env );
        
        # do something
        
        $server->response->finalize;
    };

=head1 DESCRIPTION

This class is container of server environment for Chnlr.

=head1 PROPERTIES

=head2 request_class

This property designates a request class.

The default value of this property is L<Plack::Request>.

=head2 response_class

This property designates a response class.

The default value of this property is L<Plack::Response>.

=head2 env

This property is a folder of PSGI environment hash reference.

=head2 request/req

This property is a folder of request class instance.

=head2 response/res

This propety is a folder of response class instance.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
