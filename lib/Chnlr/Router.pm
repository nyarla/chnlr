package Chnlr::Router;

use strict;
use warnings;

use Router::Simple::Declare;

my $router = router {
    # basic
    connect '/'     => {
        controller  => 'Root',
        action      => 'index',
    };

    connect '/{action:login|logout}' => {
        controller  => 'Authentication',
    };
};

sub match {
    my ( $class, @args ) = @_;
    return $router->match( @args );
}

1;

=head1 NAME

Chnlr::Router - Routing definition for Chnlr

=head1 SYNPOSIS

    use Chnlr::Router;
    
    my $param = Chnlr::Router->match( $env );

=head1 DESCRIPTION

This class is routing definition for Chnlr.

=head1 ROUTING

    # basic
    /       => controller: Root,            action: index
    
    # authenciation
    /login  => controller: Authentication,  action: login
    /logout => controller: Authentication,  action: logout

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
