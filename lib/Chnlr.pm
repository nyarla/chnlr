package Chnlr;

use strict;
use Any::Moose;
use Chnlr::Router;
use namespace::clean -except => [qw( meta )];

our $VERSION = '0.00001';

extends 'Chnlr::Context';
with    'Chnlr::Application';

sub run {
    my ( $self, $env ) = @_;

    $self->prepare($env);
    $self->dispatch($env);
    $self->finalize($env);

    return $self->server->response->finalize;
}

sub prepare {
    my ( $self, $env ) = @_;
    $self->server->env($env);
}

sub dispatch {
    my ( $self, $env ) = @_;

    my %param = ( controller => 'Root', action => 'notfound' );
    if ( my $action = Chnlr::Router->match( $env ) ) {
        %param = %{ $action };
    }

    $self->action(%param);
}

sub finalize {
    my ( $self, $env ) = @_;
}

__PACKAGE__->meta->make_immutable;

=head1 NAME

Chnlr -

=head1 SYNOPSIS

  use Chnlr;

=head1 DESCRIPTION

Chnlr is

=head1 AUTHOR

Naoki Okamrua (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
