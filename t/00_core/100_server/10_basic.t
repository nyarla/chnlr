#!perl

use strict;
use warnings;

use Test::More;
use Chnlr::Server;

use Plack::Test;
use HTTP::Request;

test_psgi(
    app => sub {
        my $env     = shift;
        my $server  = Chnlr::Server->new;
           $server->env($env);
        isa_ok( $server->req, 'Plack::Request' );
        isa_ok( $server->res, 'Plack::Response' );
        
        $server->res->status(200);
        $server->res->body('hello world');
        
        return $server->res->finalize;
    },
    client => sub {
        my $cb = shift;
        my $res = $cb->( HTTP::Request->new( GET => 'http://localhost/' ) );

        is( $res->code, 200 );
        is( $res->content, 'hello world' );
        
        warn $res->content if ( ! $res->is_success );
    },
);

done_testing;
