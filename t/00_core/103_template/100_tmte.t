#!perl

use strict;
use warnings;

use Test::More;
use t::Util qw( $examples );

BEGIN { use_ok('Chnlr::Template::TMTE') }

my $root = "${examples}/core.template.tmte";
my $tmpl = Chnlr::Template::TMTE->new( include_path => [ $root ] );

isa_ok( $tmpl, 'Chnlr::Template::TMTE' );

ok( $tmpl->exists('foo') );
ok( !  $tmpl->exists('notfound') );

my $stat = $tmpl->stat('foo');

ok( $stat->{'exists'} );
is( $stat->{'created'}, File::stat::stat("${root}/foo.mt")->ctime );
is( $stat->{'lastmodified'}, File::stat::stat("${root}/foo.mt")->mtime );

$stat = $tmpl->stat('notfound');

ok( ! $stat->{'exists'} );

is(
    $tmpl->render('foo' => 'bar'),
    'Hello bar',
);

done_testing;
