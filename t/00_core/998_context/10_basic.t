#!perl

use strict;
use warnings;

use Test::More;
use t::Util qw( $examples );
use Chnlr::Context;

my $config  = {
    server => {},
};
my $context = Chnlr::Context->new( config => $config );

# config

is_deeply(
    $context->config,
    Chnlr::Config->new( $config ),
);

$context->clear_config;

local $ENV{'CHNLR_CONFIG'} = "${examples}/core.config.new/foo.pl";

is_deeply(
    $context->config,
    Chnlr::Config->new( "${examples}/core.config.new/foo.pl" ),
);

# server

isa_ok( $context->server, 'Chnlr::Server' );

done_testing;