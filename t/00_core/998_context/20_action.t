#!perl

use strict;
use warnings;

use Test::More;
use Chnlr::Context;

{
    package MyApp;
    
    use strict;
    use Any::Moose;
    
    extends 'Chnlr::Context';

    package MyApp::Controller::Root;
    
    sub test {
        my ( $class, $context, %param )  = @_;

        main::is( $class, 'MyApp::Controller::Root' );
        main::isa_ok( $context, 'MyApp' );
        main::is_deeply(
            { %param },
            { foo => 'bar' },
        );

        return 'foo';
    }

    1;
}

my $app = MyApp->new( config => {} );

my $ret = $app->action( controller => 'MyApp::Controller::Root', action => 'test', foo => 'bar' );

is( $ret, 'foo' );

done_testing;
