#!perl

use strict;
use warnings;

use Test::More;
use Chnlr::Exceptions qw(:all);

my @tests = (
    {
        function    => 'throw',
        class       => 'Chnlr::Exception',
    },
    
    {
        function    => 'evaluate_failed',
        class       => 'Chnlr::Exception::EvaluateFailed',
    },

    {
        function    => 'invalid_parameter',
        class       => 'Chnlr::Exception::InvalidParameter',
    },
    {
        function    => 'invalid_argument_parameter',
        class       => 'Chnlr::Exception::InvalidArgumentParameter',
    },

    {
        function    => 'parameter_missing',
        class       => 'Chnlr::Exception::ParameterMissing',
    },
    {
        function    => 'argument_parameter_missing',
        class       => 'Chnlr::Exception::ArgumentParameterMissing',
    },

    {
        function    => 'not_supported',
        class       => 'Chnlr::Exception::NotSupported',
    },
    {
        function    => 'not_implemented',
        class       => 'Chnlr::Exception::NotImplemented',
    },

    {
        function    => 'io_error',
        class       => 'Chnlr::Exception::IOError',
    },
    {
        function    => 'file_io_error',
        class       => 'Chnlr::Exception::FileIOError',
    }

);

for my $test ( @tests ) {
    my ( $function, $class )
        = @{ $test }{qw( function class )};

    local $@;
    no strict 'refs';
    eval{ &{$function}( error => "test exception!" ) };
    
    isa_ok( $@, $class );
    is( $@->message, 'test exception!' );
}

done_testing;
