#!perl

use strict;
use warnings;

use Test::More;
use Plack::Test;
use HTTP::Request;

use Chnlr;

{
    package Chnlr;
    
    no strict 'refs';
    no warnings 'redefine';
    
    *{'action'} = sub {
        my ( $self, %param ) = @_;

        main::is( $param{'controller'}, 'Root' );
        main::like( $param{'action'}, qr{^index|notfound$} );

        return;
    };
}

my $app = Chnlr->new( config => {} );

test_psgi(
    app     => sub {
        my $env = shift;
        
        $app->dispatch($env);

        return [ 200, [], [] ];
    },
    client  => sub {
        my $cb = shift;
        $cb->( HTTP::Request->new( GET => 'http://localhost/' ) );
        $cb->( HTTP::Request->new( GET => 'http://localhost/xxxxxxxxxxxxxxxxxxxxxxxxxxxx' ) );
    },
);

done_testing;
