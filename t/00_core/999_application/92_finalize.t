#!perl

use strict;
use warnings;

use Test::More;
use Plack::Test;
use HTTP::Request;

use Chnlr;

my $app = Chnlr->new( config => {} );

test_psgi(
    app     => sub {
        my $env = shift;
        
        $app->finalize;
        
        ok(1);
        
        return [ 200, [], [] ];
    },
    client  => sub {
        my $cb = shift;
        
        $cb->( HTTP::Request->new( GET => 'http://localhost/' ) );
        
    },
);

done_testing;
