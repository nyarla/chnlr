#!perl

use strict;
use warnings;

use Test::More;
use Chnlr::Config;

isa_ok( 'Chnlr::Config', 'Chnlr::Variables' );

done_testing;
