#!perl

use strict;
use warnings;

use Chnlr::Config;
use Test::More;
use t::Util qw( $examples );

my $root = "${examples}/core.config.new/";
my $vars = Chnlr::Config->new;

isa_ok( $vars, 'Chnlr::Config' );
is_deeply( $vars, {} );

$vars = Chnlr::Config->new("${root}/foo.pl");

isa_ok( $vars, 'Chnlr::Config' );
is_deeply( $vars, { foo => 'AAA' } );

$vars = Chnlr::Config->new(["${root}/foo.pl", "${root}/bar.pl"]);

isa_ok( $vars, 'Chnlr::Config' );
is_deeply( $vars, { foo => 'AAA', bar => 'BBB' } );

$vars = Chnlr::Config->new({ baz => 'CCC' });

isa_ok( $vars, 'Chnlr::Config' );
is_deeply( $vars, { baz => 'CCC' } );

$vars = Chnlr::Config->new(sub { return { AAA => 'BBB' } });

isa_ok( $vars, 'Chnlr::Config' );
is_deeply( $vars, { AAA => 'BBB' } );

done_testing;
