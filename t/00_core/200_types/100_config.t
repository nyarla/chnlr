#!perl

use strict;
use warnings;

use Test::More;
use t::Util qw( $examples );
use Chnlr::Types qw( Config );

my $root = "${examples}/core.config.new";
my $meta = Config;

is( "${meta}", 'Chnlr::Types::Config' ) ;

ok( $meta->check( Chnlr::Config->new ) );
ok( ! $meta->check( {} ) );

is_deeply(
    $meta->coerce("${root}/foo.pl"),
    Chnlr::Config->new("${root}/foo.pl"),
);

is_deeply(
    $meta->coerce(["${root}/foo.pl", "${root}/bar.pl"]),
    Chnlr::Config->new(["${root}/foo.pl", "${root}/bar.pl"]),
);

is_deeply(
    $meta->coerce([{ foo => 'bar'}, { bar => 'baz' }]),
    Chnlr::Config->new({ foo => 'bar', bar => 'baz' }),
);

is_deeply(
    $meta->coerce(["${root}/foo.pl", { bar => 'baz' }]),
    Chnlr::Config->new({ foo => 'AAA', bar => 'baz' }),
);

is_deeply(
    $meta->coerce({ foo => 'bar' }),
    Chnlr::Config->new({ foo => 'bar' }),
);

is_deeply(
    $meta->coerce(sub { return "${root}/foo.pl" }),
    Chnlr::Config->new("${root}/foo.pl"),
);

done_testing;
