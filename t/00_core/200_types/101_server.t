#!perl

use strict;
use warnings;

use Test::More;
use Chnlr::Types qw( Server );

my $meta = Server;

is( "${meta}", 'Chnlr::Types::Server' );

ok( $meta->check( Chnlr::Server->new ) );
ok( ! $meta->check({}) );

is_deeply(
    $meta->coerce({}),
    Chnlr::Server->new(),
);

done_testing;
