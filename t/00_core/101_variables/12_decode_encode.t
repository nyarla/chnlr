#!perl

use strict;
use warnings;

use Chnlr::Variables;
use Test::More;

my $vars = Chnlr::Variables->new({
    foo => '日本語',
    bar => 'foobar',
});

$vars->decode('utf8');

ok( utf8::is_utf8( $vars->{'foo'} ) );

$vars->encode('utf8');

ok( ! utf8::is_utf8( $vars->{'foo'} ) );

done_testing;
