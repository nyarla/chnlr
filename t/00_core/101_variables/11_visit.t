#!perl

use strict;
use warnings;

use Chnlr::Variables;
use Test::More;

my $vars = Chnlr::Variables->new({
    foo => 'AAA',
    bar => [qw( BBB BBB )],
    baz => {
        AAA => 'FOO',
        BBB => [qw( BAR BAR )],
    },
});

$vars->visit(sub { return lc $_[0] });

is_deeply(
    $vars,
    {
        foo => 'aaa',
        bar => [qw( bbb bbb )],
        baz => {
            AAA => 'foo',
            BBB => [qw( bar bar )],
        },
    },
);

done_testing;
