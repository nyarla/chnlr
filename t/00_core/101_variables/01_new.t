#!perl

use strict;
use warnings;

use Chnlr::Variables;
use Test::More;

my $vars = Chnlr::Variables->new;

isa_ok( $vars, 'Chnlr::Variables' );

$vars = Chnlr::Variables->new({ foo => 'bar' });

isa_ok( $vars, 'Chnlr::Variables' );

is_deeply(
    $vars,
    {
        foo => 'bar',
    }
);

done_testing;
