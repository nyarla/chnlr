#!perl

use strict;
use warnings;

use Chnlr::Variables;
use Test::More;

my $vars = Chnlr::Variables->new({ foo => 'bar', bar => { baz => 'AAA' } });

$vars->merge(
    {
        baz => 'BBB',
    },
    {
        bar => {
            foo => 'CCC',
        }
    },
);

is_deeply(
    $vars,
    {
        foo => 'bar',
        baz => 'BBB',
        bar => {
            baz => 'AAA',
            foo => 'CCC',
        },
    },
);

done_testing;
