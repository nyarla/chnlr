#!perl

use strict;
use warnings;

use Test::More;
use Chnlr::Router;

my @tests = (
    # basic
    {
        path    => '/',
        param   => {
            controller  => 'Root',
            action      => 'index',
        }
    },

    {
        path    => '/login',
        param   => {
            controller  => 'Authentication',
            action      => 'login',
        },
    },
    {
        path    => '/logout',
        param   => {
            controller  => 'Authentication',
            action      => 'logout',
        },
    },
);

for my $test ( @tests ) {
    my ( $path, $param ) = @{ $test }{qw( path param )};
    is_deeply(
        Chnlr::Router->match($path),
        $param,
    );
}

done_testing;
