use inc::Module::Install;

use strict;
use warnings;

name 'Chnlr';
all_from 'lib/Chnlr.pm';

# OO
requires 'Any::Moose';
requires 'namespace::clean';
requires 'Mouse';
requires 'MouseX::Types';
requires 'parent';

# Plack
requires 'Plack';
requires 'Plack::Request';
requires 'Plack::Response';

# Data Utility
requires 'Hash::Merge::Simple';
requires 'Data::Rmap';
requires 'Encode';

# basic
test_requires 'Test::More';

# psgi test
test_requiers 'Plack::Test';
test_requires 'HTTP::Request';

tests 't/*/*/*.t';
author_tests('xt');

auto_include;
auto_install;

WriteAll;
